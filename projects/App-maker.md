## Admin Application Maker

<sup> GIF size: 13MB, it's coming, wait for a moment 😋</sup>

![](../assets/app-maker-demo.gif)

<br>

### Background


There's so many Requirements for the Forms and Tables and they are always cost developers' time to do these easy but time-consuming works, **Admin Application Maker** allow developer assemble an App by Drag and Drop interactions with very less code to write, even don't have to `create repo` -> `git flow` -> `ci` -> `deploy`, all is visual.

<br>
<br>
<br>
<br>