## Online User Feedback Tool


### Features:
+ [Record](https://github.com/waynecz/session-recorder) every movement in page and replay at player
+ [Record](https://github.com/waynecz/session-recorder) every critical actions (requests, errors, console)

<br>
<sup> GIF size: 31MB, it's coming, wait for a moment 😋</sup>

### Record: 

![](../assets/record.gif)

<br>

### Replay:

![](../assets/replay.gif)


### See More about:

![](../assets/player.gif)

<br>
<br>
<br>